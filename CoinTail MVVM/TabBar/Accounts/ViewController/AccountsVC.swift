//
//  AccountsVC.swift
//  CoinTail MVVM
//
//  Created by Eugene on 14.03.24.
//

import UIKit
import EasyPeasy


final class AccountsVC: BasicVC {
    
    let viewModel = AccountsVM()
    
    var accounts: [AccountClass]!

    static let noAccountsText = "You have no accounts added"
    static let accountsDescriptionText = "Here you can add different money storage methods, such as cards, cash or a bank deposit"
    
    let noAccountsLabel: UILabel = getNoDataLabel(text: noAccountsText)
    let accountsDescriptionLabel: UILabel = getDataDescriptionLabel(text: accountsDescriptionText)
    let accountsEmojiLabel: UILabel = getDataEmojiLabel("🧐")
    let addAccountButton: UIButton = getAddDataButton(text: "Add an account")
    
    let transferButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named: "primaryAction")
        button.layer.cornerRadius = 16
        button.setTitle("Transfer money", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font.withSize(17)
        
        return button
    }()
    var historyButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named: "secondaryAction")
        button.layer.cornerRadius = 16
        button.setTitle("Transfer history", for: .normal)
        button.setTitleColor(UIColor(named: "primaryAction"), for: .normal)
        button.titleLabel?.font.withSize(17)

        return button
    }()
    
    var accountsCV: UICollectionView = {
        let accountsLayout: UICollectionViewFlowLayout = {
            let layout = UICollectionViewFlowLayout()
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            
            return layout
        }()
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: accountsLayout)
        cv.contentInset = .init(top: 32, left: 0, bottom: 0, right: 0) // Отступ сверху
        cv.backgroundColor = .clear
//        cv.register(AccountCell.self, forCellWithReuseIdentifier: AccountCell.id)
        cv.cornerRadiusBottom(radius: 12)
        
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.alwaysBounceVertical = true
        
        return cv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar.titleLabel.text = "Accounts"
        
        navigationController?.delegate = self
        
        bindAccountsVM()
        accountButtonTargets()
        accountsSubviews()
        emptyDataSubviews(
            dataView: accountsEmojiLabel,
            noDataLabel: noAccountsLabel,
            dataDescriptionLabel: accountsDescriptionLabel,
            addDataButton: addAccountButton
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.setAccounts()
        isAccountEmpty()        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        navigationController?.navigationBar.isHidden = false
    }
    
}

extension AccountsVC: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController is AccountsVC {
            navigationController.setNavigationBarHidden(true, animated: animated)
        } else {
            navigationController.setNavigationBarHidden(false, animated: animated)
        }
    }

    func accountsSubviews() {
        self.view.addSubview(accountsCV)
        self.view.addSubview(customNavBar)
        self.view.addSubview(transferButton)
        self.view.addSubview(historyButton)
        
        customNavBar.easy.layout([
            Height(96),
            Top().to(self.view.safeAreaLayoutGuide, .top),
            Left(),
            Right()
        ])
        
        historyButton.easy.layout([
            Height(52),
            Left(16),
            Right(16),
            CenterX(),
            Bottom(24).to(self.view.safeAreaLayoutGuide, .bottom)
        ])
        
        transferButton.easy.layout([
            Height(52),
            Left(16),
            Right(16),
            CenterX(),
            Bottom(16).to(historyButton, .top)
        ])

        accountsCV.easy.layout([
            Top().to(customNavBar, .bottom),
            CenterX(),
            Left(16),
            Right(16),
            Bottom(24).to(transferButton, .top)
        ])
    }
    
    func isAccountEmpty() {
        let isEmpty = accounts.isEmpty
        
        accountsEmojiLabel.isHidden = !isEmpty
        noAccountsLabel.isHidden = !isEmpty
        accountsDescriptionLabel.isHidden = !isEmpty
        addAccountButton.isHidden = !isEmpty
        emptyDataView.isHidden = !isEmpty
        
        transferButton.isHidden = isEmpty
        historyButton.isHidden = isEmpty
        accountsCV.isHidden = isEmpty
    }
    
    func accountButtonTargets() {
//        historyButton.addTarget(self, action: #selector(goToAccountsHistoryVC), for: .touchUpInside)
//        transferButton.addTarget(self, action: #selector(goToAccountsTransferVC), for: .touchUpInside)
        addAccountButton.addTarget(viewModel, action: #selector(AccountsVM.goToAddAccount), for: .touchUpInside)
    }
    
    func bindAccountsVM() {
        viewModel.accountsBind.bind { [weak self] accounts in
            guard let strongSelf = self else { return }
            
            strongSelf.accounts = accounts
        }
    }
    
}
