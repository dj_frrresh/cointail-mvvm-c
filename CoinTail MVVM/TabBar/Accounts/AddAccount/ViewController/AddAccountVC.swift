//
//  AddAccountVC.swift
//  CoinTail MVVM
//
//  Created by Eugene on 11.04.24.
//

import UIKit
import RealmSwift
import EasyPeasy


final class AddAccountVC: PickerVC {
    
    let viewModel = AddAccountVM()
        
    var selectedCurrency: String = "USD"
//    {
//        didSet {
//            let indexPathToUpdate = IndexPath(item: 2, section: 0)
//            
//            updateCell(at: indexPathToUpdate, text: selectedCurrency)
//        }
//    }
    
    var accountID: ObjectId?
    var accountName: String?
    var accountAmount: String?
    var isAccountMain: Bool = true
    
    let favouriteStringCurrencies: [String] = ["RUB", "EUR"]

    let deleteAccountButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named: "cancelAction")
        button.layer.cornerRadius = 16
        button.setTitle("Delete account", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "SFProDisplay-Semibold", size: 17)
        button.isHidden = true
        
        return button
    }()
    
    let addAccountCV: UICollectionView = {
        let addAccountLayout: UICollectionViewFlowLayout = {
            let layout = UICollectionViewFlowLayout()
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0

            return layout
        }()

        let cv = UICollectionView(frame: .zero, collectionViewLayout: addAccountLayout)
        cv.backgroundColor = .clear
//        cv.register(AddAccountCell.self, forCellWithReuseIdentifier: AddAccountCell.id)

        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.alwaysBounceVertical = false
        cv.isScrollEnabled = false

        return cv
    }()
    
    init(accountID: ObjectId) {
        self.accountID = accountID

        super.init(nibName: nil, bundle: nil)

        self.title = "Edit account"
        
        guard let account = Accounts.shared.getAccount(for: accountID) else { return }
//        setupUI(with: account)
    }
    
    public required init() {
        super.init(nibName: nil, bundle: nil)
        
        self.title = "Add a new account"
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
                        
//        itemsPickerView.dataSource = self
//        addAccountCV.dataSource = self
//
//        itemsPickerView.delegate = self
//        addAccountCV.delegate = self
        
        addAccountNavBar()
        addAccountSubviews()
        setupToolBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false

        addAccountTargets()
    }
    
}

extension AddAccountVC {
    
    func setupUI(with account: AccountClass) {
        let formattedAmount = String(format: "%.2f", account.startBalance)
        accountAmount = "\(formattedAmount)"
        accountName = account.name
        selectedCurrency = account.currency
        
        deleteAccountButton.isHidden = false
    }
    
    func addAccountSubviews() {
        self.view.addSubview(addAccountCV)
        self.view.addSubview(deleteAccountButton)
        
        addAccountCV.easy.layout([
            Left(16),
            Right(16),
            Top(32).to(self.view.safeAreaLayoutGuide, .top),
            Height(48 * 3)
        ])
        
        deleteAccountButton.easy.layout([
            Left(16),
            Right(16),
            Top(24).to(addAccountCV, .bottom),
            Height(52)
        ])
    }
    
    func addAccountNavBar() {
        let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: viewModel, action: #selector(AddAccountVM.test))
        let backButton = UIBarButtonItem(title: "Back", style: .plain, target: viewModel, action: #selector(AddAccountVM.goToBackVC))
            
        self.navigationItem.rightBarButtonItem = saveButton
        self.navigationItem.leftBarButtonItem = backButton
    }
    
    func addAccountTargets() {
        deleteAccountButton.addTarget(viewModel, action: #selector(AddAccountVM.test), for: .touchUpInside)
    }

    func updateCell(at indexPath: IndexPath, text: String) {
//        if let cell = addAccountCV.cellForItem(at: indexPath) as? AddAccountCell {
//            cell.updateCurrencyLabel(text)
//        }
    }
    
}

