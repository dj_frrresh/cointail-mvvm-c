//
//  AccountsVM.swift
//  CoinTail MVVM
//
//  Created by Eugene on 26.03.24.
//

import UIKit


class AccountsVM {
    
    var accountsBind: Bindable<[AccountClass]> = Bindable([])

    func setAccounts() {
        accountsBind.value = RealmService.shared.accountsArr
    }
    
    @objc func goToAddAccount() {
        guard let appCoordinator = appCoordinator else { return }

        appCoordinator.goToNextVC(newVC: AddAccountVC(), useNavigationController: true)
    }
    
}
