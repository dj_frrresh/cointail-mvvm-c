//
//  TabBarDelegate.swift
//  CoinTail MVVM
//
//  Created by Eugene on 14.03.24.
//

import UIKit


// Плавный переход между контроллерами на TabBar'е
extension TabBar: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let fromView = selectedViewController?.view,
              let toView = viewController.view else { return false }

        if fromView != toView {
            UIView.transition(from: fromView, to: toView, duration: 0.3, options: [.transitionCrossDissolve], completion: nil)
        }
        
        return true
    }
    
}
