//
//  TabBar.swift
//  CoinTail MVVM
//
//  Created by Eugene on 14.03.24.
//

import UIKit


class TabBar: UITabBarController {
    
    private let accountsVC = UINavigationController(rootViewController: AccountsVC())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        // Установка контроллеров на TabBar
        self.setViewControllers([accountsVC], animated: false)
        
        setAppearance()
    }
    
    private func setAppearance() {
        accountsVC.title = "Accounts"
        
        UITabBar.appearance().backgroundColor = UIColor(named: "TabBarBackground") // Цвет фона TabBar'а
        UITabBar.appearance().tintColor = UIColor(named: "primaryAction") // Цвет выбранной иконки и текста
        UITabBar.appearance().unselectedItemTintColor = UIColor(named: "unselectedScreen") // Цвет невыбранных иконок
        
        guard let items = self.tabBar.items else { return }
        let images = ["creditcard.fill"]
        
        // Установка иконок
        for i in 0..<items.count {
            items[i].image = UIImage(systemName: images[i])
        }
    }
    
}
