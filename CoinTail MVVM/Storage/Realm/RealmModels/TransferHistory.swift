//
//  TransferHistory.swift
//  CoinTail MVVM
//
//  Created by Eugene on 14.03.24.
//

import RealmSwift


class TransferHistoryClass: Object {
    @Persisted(primaryKey: true) var id: ObjectId
    
    @Persisted var sourceAccount: String = ""
    @Persisted var targetAccount: String = ""
    @Persisted var sourceCurrency: String = ""
    @Persisted var targetCurrency: String = ""
    @Persisted var sourceAmount: Double = 0
    @Persisted var targetAmount: Double = 0
    @Persisted var date: Date = Date()
}
