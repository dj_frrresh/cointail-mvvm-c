//
//  Account.swift
//  CoinTail MVVM
//
//  Created by Eugene on 14.03.24.
//

import Foundation
import RealmSwift


class AccountClass: Object, ObjectKeyIdentifiable {
    @Persisted(primaryKey: true) var id: ObjectId
    
    @Persisted var name: String = ""
    @Persisted var startBalance: Double = 0
    @Persisted var amountBalance: Double = 0
    @Persisted var currency: String = ""
}
