//
//  RealmService.swift
//  CoinTail MVVM
//
//  Created by Eugene on 14.03.24.
//

import Foundation
import RealmSwift


final class RealmService {
    
    static let shared = RealmService()
        
    private(set) var realm: Realm?
    
    // Здесь хранятся все объекты классов из базы данных
    var transfersHistoryArr: [TransferHistoryClass] = []
    var accountsArr: [AccountClass] = []

    private init() {
        realmInit()
    }
    
    func write<T: Object>(_ object: T, _ type: T.Type) {
        do {
            try realm?.write({
                realm?.add(object)
            })
        } catch let error {
            print(error)
        }
        
        RealmService.shared.readAll(type)
    }
    
    func read<T: Object>(_ object: T.Type) -> [T] {
        return realm?.objects(object).map { $0 } ?? []
    }
    
    func readAll<T: Object>(_ object: T.Type) {
        switch "\(object)" {
        case "TransferHistoryClass":
            transfersHistoryArr.removeAll()
        case "AccountClass":
            accountsArr.removeAll()
        default:
            fatalError("Error when trying to read all realm objects")
        }
        
        if let objects = realm?.objects(object.self).map({$0}) {
            for object in objects {
                switch object {
                case let transfersHistory as TransferHistoryClass:
                    transfersHistoryArr.append(transfersHistory)
                case let account as AccountClass:
                    accountsArr.append(account)
                default:
                    fatalError()
                }
            }
        }
    }
    
    func update<T: Object>(_ object: T, _ type: T.Type) {
        do {
            try realm?.write {
                realm?.add(object, update: .modified)
            }
        } catch let error {
            print(error)
        }

        RealmService.shared.readAll(type)
    }
    
    func delete<T: Object>(_ object: T, _ type: T.Type) {
        do {
            try realm?.write {
                realm?.delete(object)
            }
        } catch let error {
            print(error)
        }
        
        RealmService.shared.readAll(type)
    }
    
    func deleteAllObjects<T: Object>(_ type: T.Type) {
        guard let objects = realm?.objects(type) else { return }
        
        do {
            try realm?.write {
                realm?.delete(objects)
            }
        } catch let error {
            print(error)
        }
        
        RealmService.shared.readAll(type)
    }
    
    func readAllClasses() {
        RealmService.shared.readAll(AccountClass.self)
        RealmService.shared.readAll(TransferHistoryClass.self)
    }
    
    func deleteAllData() {
        deleteAllObjects(AccountClass.self)
        deleteAllObjects(TransferHistoryClass.self)
    }
    
    private func realmInit() {
        do {
            // Запись, необходимая для миграции данных
            let config = Realm.Configuration(schemaVersion: 1)
            Realm.Configuration.defaultConfiguration = config
            
            realm = try Realm()
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
}
