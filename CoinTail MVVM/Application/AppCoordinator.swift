//
//  AppCoordinator.swift
//  CoinTail MVVM
//
//  Created by Eugene on 11.04.24.
//

import UIKit


class AppCoordinator {
    
    private let window: UIWindow
    private var viewControllers: [UIViewController] = []
    
    init(window: UIWindow) {
        self.window = window
    }
    
    // Задаем начальный контроллер
    func start() {
        let vc = TabBar()
        
        window.rootViewController = vc
        window.makeKeyAndVisible()
        viewControllers.append(vc)
    }
    
    // Переходим на новый контроллер
    func goToNextVC(newVC: BasicVC, useNavigationController: Bool = false) {
        let defaultAnimation = {
            UIView.transition(with: self.window, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: nil)
        }
        
        // Выбираем навигационный контроллер или обычный
        let vc: UIViewController = useNavigationController ? UINavigationController(rootViewController: newVC) : newVC
        
        viewControllers.append(vc)
        defaultAnimation()
        window.rootViewController = vc
    }
    
    // Переходим на предыдущий контроллер
    func goBackVC() {
        guard viewControllers.count > 1 else { return } // Убедимся, что есть что возвращать
        
        let defaultAnimation = {
            UIView.transition(with: self.window, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: nil)
        }
        
        viewControllers.removeLast() // Удаляем текущий контроллер
        let previousVC = viewControllers.last! // Получаем предыдущий контроллер
        defaultAnimation()
        window.rootViewController = previousVC
    }
    
}
