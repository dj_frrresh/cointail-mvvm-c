//
//  AppDelegate.swift
//  CoinTail MVVM
//
//  Created by Eugene on 14.03.24.
//

import UIKit


// Задаем главный координатор на все приложение
var appCoordinator: AppCoordinator?

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow? // Окно приложения, координирует представление изображения

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        
        appCoordinator = AppCoordinator(window: window)
        appCoordinator?.start()
        
        RealmService.shared.readAllClasses() // Читаем все Realm-классы
        
        return true
    }

}

