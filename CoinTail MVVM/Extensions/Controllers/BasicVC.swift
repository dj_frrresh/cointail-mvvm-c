//
//  BasicVC.swift
//  CoinTail MVVM
//
//  Created by Eugene on 26.03.24.
//

import UIKit
import EasyPeasy


class BasicVC: UIViewController {
    
    var prefersLargeTitle: Bool = false
    
    let emptyDataView = UIView()
    let customNavBar = CustomNavigationBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Убрать клавиатуру при нажатии на экран
        setupHideKeyboardOnTap()
                
        self.view.backgroundColor = UIColor(named: "AccentColor")
        self.navigationController?.navigationBar.tintColor = .systemBlue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = prefersLargeTitle
        self.navigationController?.navigationBar.layoutIfNeeded()
        self.navigationController?.navigationBar.sizeToFit()

        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.layoutIfNeeded()
        
        super.viewWillDisappear(animated)
    }
    
}

extension BasicVC {
    
    static func getAddDataButton(text: String) -> UIButton {
        let button = UIButton()
        button.backgroundColor = UIColor(named: "primaryAction")
        button.layer.cornerRadius = 16
        button.setTitle(text, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17)

        return button
    }
    static func getNoDataLabel(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.font = UIFont.systemFont(ofSize: 28)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        
        return label
    }
    static func getDataDescriptionLabel(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.font = UIFont.systemFont(ofSize: 17)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.textColor = UIColor(named: "secondaryTextColor")
        
        return label
    }
    
    static func getDataEmojiLabel(_ emoji: String) -> UILabel {
        let label = UILabel()
        label.text = emoji
        label.font = UIFont.systemFont(ofSize: 100)
        label.numberOfLines = 1
        label.textAlignment = .center
        
        return label
    }
    
    static func getDataImageView(name: String) -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: name)

        return imageView
    }
    
    func emptyDataSubviews(dataView: UIView, noDataLabel: UILabel, dataDescriptionLabel: UILabel, addDataButton: UIButton) {
        self.view.addSubview(emptyDataView)
        
        emptyDataView.easy.layout([
            Left(16),
            Right(16),
            Center(),
            Height(BasicVC.noDataViewSize(
                noDataLabel: noDataLabel,
                descriptionLabel: dataDescriptionLabel
            ))
        ])

        emptyDataView.addSubview(dataView)
        emptyDataView.addSubview(noDataLabel)
        emptyDataView.addSubview(dataDescriptionLabel)
        emptyDataView.addSubview(addDataButton)
        
        dataView.easy.layout([
            Height(100),
            Width(100),
            Top(),
            CenterX()
        ])
        
        noDataLabel.easy.layout([
            Left(),
            Right(),
            Top(32).to(dataView, .bottom)
        ])
        
        dataDescriptionLabel.easy.layout([
            Left(),
            Right(),
            Top(16).to(noDataLabel, .bottom)
        ])

        addDataButton.easy.layout([
            Height(52),
            Left(),
            Right(),
            CenterX(),
            Top(16).to(dataDescriptionLabel, .bottom),
            Bottom()
        ])
    }
    
    // Отображает динамически высоту для View
    static private func noDataViewSize(noDataLabel: UILabel, descriptionLabel: UILabel) -> CGFloat {
        let padding: CGFloat = 16.0
        let imagePadding: CGFloat = 32.0
        
        let textWidth = UIScreen.main.bounds.width - (2 * padding)
    
        let noDataLabelHeight: CGFloat = noDataLabel.sizeThatFits(.init(width: textWidth, height: 0)).height
        let descriptionHeight: CGFloat = descriptionLabel.sizeThatFits(.init(width: textWidth, height: 0)).height
        
        let imageViewHeight: CGFloat = 100.0
        let buttonHeight: CGFloat = 52.0
        
        let viewHeight = imageViewHeight + imagePadding + noDataLabelHeight + descriptionHeight + padding * 2 + buttonHeight
                        
        return viewHeight
    }
    
}

extension BasicVC {
    
    // Настройка title для контроллера
    func setupNavigationTitle(title: String, large: Bool = false) {
        prefersLargeTitle = large
        navigationController?.navigationBar.prefersLargeTitles = large
        navigationItem.largeTitleDisplayMode = large ? .always : .never
        
        if large {
            navigationItem.title = title
            navigationItem.setValue(1, forKey: "__largeTitleTwoLineMode")
            
            
        } else {
            let titleLabel: UILabel = {
                let label = UILabel()
                label.text = title
                label.textAlignment = .center
                label.font = UIFont(name: "SFProDisplay-Semibold", size: 17)
                
                return label
            }()
            
            navigationItem.titleView = titleLabel
        }
    }
    
    // Убрать клавиатуру при нажатии на экран за пределы клавиатуры
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(endEditingRecognizer())
        navigationController?.navigationBar.addGestureRecognizer(endEditingRecognizer())
    }
    // Регистрирует нажатия на экран
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        
        return tap
    }
    
}

extension BasicVC: UITextFieldDelegate {
    
    // Обработка нажатия Return на клавиатуре
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // Закрыть клавиатуру
        
        return true
    }
    
}
