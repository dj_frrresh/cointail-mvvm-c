//
//  PickerVC.swift
//  CoinTail MVVM
//
//  Created by Eugene on 11.04.24.
//

import UIKit
import EasyPeasy


class PickerVC: BasicVC {
    
    var isPickerHidden: Bool = true
    
    let pickerToolBar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.isHidden = true
        toolbar.sizeToFit()
        toolbar.tintColor = .systemBlue
        toolbar.backgroundColor = .white

        return toolbar
    }()
        
    let itemsPickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.isHidden = true
        picker.backgroundColor = .white
        
        return picker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerSubview()
    }
    
}

extension PickerVC {
    
    func pickerSubview() {
        self.view.addSubview(itemsPickerView)
        self.view.addSubview(pickerToolBar)
        
        itemsPickerView.easy.layout([
            Left(),
            Right(),
            Height(200),
            Bottom(-300).to(self.view.safeAreaLayoutGuide, .bottom)
        ])
        
        pickerToolBar.easy.layout([
            Left(),
            Right(),
            Height(44),
            Bottom().to(itemsPickerView, .top)
        ])
    }
    
    func setupToolBar() {
        let doneButton = UIBarButtonItem(
            barButtonSystemItem: .done,
            target: self,
            action: #selector(doneButtonAction)
        )

        pickerToolBar.setItems([doneButton], animated: true)
    }
    
    @objc func doneButtonAction() {
        hidePickerView()
    }
    
    func showPickerView() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.itemsPickerView.isHidden = false
            strongSelf.pickerToolBar.isHidden = false
            strongSelf.itemsPickerView.easy.layout(
                Bottom()
            )
            strongSelf.view.layoutIfNeeded()
        }
        isPickerHidden = false
    }
    
    func hidePickerView() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.itemsPickerView.easy.layout(
                Bottom(-300).to(strongSelf.view.safeAreaLayoutGuide, .bottom)
            )
            strongSelf.view.layoutIfNeeded()
        } completion: { [weak self] _ in
            self?.itemsPickerView.isHidden = true
            self?.pickerToolBar.isHidden = true
        }
        isPickerHidden = true
    }
    
}

